# Ether Mashup Device


The **Ether Mashup Device** is a portable device that sniff, decrypt and store images or webpages from a selected wifi network. It has also the ability to send all the captured traffic to a remote server. Data can be shown by running the [Ether Mashup Server](https://bitbucket.org/clemsos/ethermashup-ui) on the remote machine.

The code featured here is part of **The Ether Mashup**, an art project about network surveillance by Clément Renaud, Lionel Radisson and Nicolas Maigret.

## In this rep

  * ```env.sh``` the config file 
  * ```./sniffer``` sniff  data from a selected wep/wpa2 network  
  * ```./writer``` write files on the hard disk
  * ```./sender``` watch folder and send new files to another machine using scp (ssh)
  * ```./sender_secure``` identical but use tar compression and ssh/netcat for better results
  * ```http_parser.py``` grab http traffic and store files using the awesome [Justniffer](http://justniffer.sourceforge.net)  (requires ```common.py```)
  * ```start.sh``` a startup script to launch all the process
  * ```ethersniffer``` is a daemon script to implement sniffer as a service

The ```/docs``` rep contains some more info and notes about the development process. It is a mess but it may come in handy to someone who wants to dig into the config and install.


## Install

To install it, you need : 

    * A mini-pc (tested with raspberry Pi but should work with any Debian-like environment)
    * SD card (the faster the better - tested with Kingston 16GB Class10)
    * USB Wifi Card 1 (monitoring tested with TP-LINK TL-WN722N)
    * USB Wifi Card 2 (data transfer tested with EDUP EP-N8508GS)
    * Ethernet cable

### Quick Install on Raspberry Pi

* Plug the 2 wifi in usb ports (big TP-LINK at the bottom, small wifi key at the top)
* Download the Image [pisniffer_20131003.img]()
* Flash the image to your SD card
* SSH the pi to your computer with the ethernet cable : ```ssh pi@192.168.1.202``` 
* Password is ```raspberry```
* Configure values in ```env.sh``` to sniff a network and send to server
* Authorize your device for password-free ssh login into your server ```ssh-copy-id user@server_ip```
* Reboot the machine (the boot sequence can take up to one minute)

### Install on other systems, hardware and platforms
  
See in the ```/docs``` folder

  * OpenWRT : tested on TP-LINK TL-WR703N -- sniffing works but decryption doesn't (not enough computing power)
  * Ubuntu : works on Ubuntu


## Exhibit

You will find instructions about how to exhibit in the file ```/docs/exhibit.md``` [read now](docs/exhibit.md)



