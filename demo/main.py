#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bottle import route, run, template, get, debug, static_file
import os, time
import base64
import json
# debug(True)


# import sys
# sys.setdefaultencoding("utf-8")

img_types=["jpg","png","gif"]
# file_rep="/home/clemsos/Dev/ethermashup/data/datafromsniff"
file_rep="/tmp/data"

img_rep=os.path.join(os.path.dirname(os.path.realpath(__file__)), "images")
if not os.path.lexists(img_rep):
    os.symlink(file_rep, img_rep)

def generate_data():
    data=[]
    for path, subdirs, files in os.walk(file_rep):
     for filename in files:
        try:
            mypath=os.path.split(path)[1]
            imgpath=os.path.join("images",mypath)
            f = os.path.join(imgpath, filename)
            # print f
            if filename[-3:] in img_types:
                fin=('<img src="'+ f + '" />').decode('utf-8')
                data.append({"path":fin})
        except UnicodeDecodeError:
            pass

    return {"images":data}

@get('/<filename:re:.*\.(jpg|png|gif|ico)>')
def images(filename):
    f=os.path.join(os.path.split(os.path.split(filename)[0])[1],os.path.split(filename)[1])
    return static_file(f, root=file_rep)

@route('/<filename:re:.*\.(css)>')
def server_static(filename):
    return static_file(filename, root=os.getcwd())

@route('/')
def index():
    return template('index')

@route('/images.json')
def images_getallitems():
    data = generate_data()
    # print data
    # return json.dumps(data, ensure_ascii=False)
    return data

run(host='localhost', port=8080)