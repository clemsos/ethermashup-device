#!/bin/bash

# local
export ETHER_DATA_DIR=/tmp/data
export ETHER_FILELIST=/tmp/filelist
export ETHER_DEST_DIR=/tmp/data/test

# logs
export ETHER_USER=nico # should have rights to write on /tmp and logs
export ETHER_REP=/home/$ETHER_USER/ethermashup-device

# network to sniff
export ETHER_SSID="Maison Populaire Pollux"
# ETHER_PSK="12345abcde"
export ETHER_CRYPTO="None" # available options : "WEP","WPA", "None"
export ETHER_CHANNEL="11"
export ETHER_BSSID=00:60:1D:F7:06:10 # TO FIND BSSID : airodump-ng mon0


# server
export ETHER_DEST_USER=clemsos #ether
export ETHER_DEST_IP=192.168.1.111 #103.5.12.91
export ETHER_DEST_DIR=/tmp/data
