#!/bin/bash

source env.sh

USER=$ETHER_USER
REP=/home/$USER/ethermashup-device
OUT1=$REP/logs/sniffer.log
OUT2=$REP/logs/writer.log
OUT3=$REP/logs/sender.log

if [[ -z ${OUT1} ]]; then touch ${OUT1}; fi

echo "starting sniffer "
sudo  ${REP}/sniffer start > $OUT1 2>$OUT1 &
sleep 30
echo "starting writer "
sudo  ${REP}/writer start > $OUT2 2>$OUT2 &
sleep 15
echo "starting transfer"
exec sudo -u $USER ${REP}/sender start > $OUT3 2>$OUT3 &
 
exit 0
