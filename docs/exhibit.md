# HOWTO Exhibit Ether Mashup 

Here are some tips and solutions to display EtherMashup on a computer in an exhibition

### Exhibit with Windows
* install AutoHideMousePointer.exe [Download link](http://dw.cbsi.com/redir?edId=3&siteId=4&oId=3000-2084_4-75676781&ontId=2084_4&spi=1ffde927ca40a64178f49c761433877c&lop=link&tag=tdw_dltext&ltype=dl_dlnow&pid=13302887&mfgId=6306246&merId=6306246&pguid=UkY-uQoOYJMAADv8cB4AAAA7&ttag=dl_dldirect&ctype=language&cval=en&destUrl=http%3A%2F%2Fdownload.cnet.com%2F3001-2084_4-75676781.html%3Fspi%3D1ffde927ca40a64178f49c761433877c%26dlm%3D0) to hide mouse pointer
* Download and install Chrome [Download link](https://www.google.com/intl/en/chrome/browser/)
* Setup URL to chrome default homepage : Settings > On startup > Open a specific page or set of pages > Set pages > 
* create a startup.bat file containing the following script
* add this script to Startup Menu > Startup

```
#!bat
REM     Change to the directory of google chrome. This path will be different based on your system. A quick way to find it is to copy it from an already existing
REM     shortcut of chrome. Usually there’s one installed in the start menu or quick launch bar.
cd C:\Users\[username]\AppData\Local\Google\Chrome\Application\
REM     Launch the application. Don’t worry about any parameters, as we’re going to quit it in a few seconds anyway.
REM     I used the “start” method so we continue through the script.
start chrome.exe
REM     Put a delay in here of a few seconds. Like Sean Connery in Red October, I used one ping. Depending on system performance, you may
REM     want two pings. Just has to be enough time to start the application.
ping localhost
REM   Quit chrome. The /IM tells it to quit gracefully, and not just a inelegant end process.
Taskkill /IM chrome.exe
REM     Delay us another few seconds to allow the app to quit.
ping localhost
REM     Now, launch chrome in kiosk mode. It should launch fullscreen, with no warning message.
chrome.exe –kiosk
```



### Exhibit with Android

You need those apps :
* Startup Manager
* Full!Screen (needs root)

### Exhibit with iPad 
You need those apps
* Kiosk