
# Delete everthing
mtd -r erase rootfs_data

###########################
# Install OpenWRT
###########################

# connect to device
telnet 192.168.1.1

# configure root passwd
passwd
exit

# connect to device
ssh root@192.168.1.1 

# install OpenWRT, add network & Wifi
# http://wiki.xinchejian.com/wiki/Install_OpenWRT_on_TPlink_WR703N

########################
# Configure Network
########################

vi /etc/config/network 
    
    config interface 'wan'
        option ifname 'wlan0'
        option proto 'dhcp' 

vi /etc/config/wireless 

    config wifi-iface
        option network 'wan'
        option ssid 'RAWR! N'
        option encryption 'psk2'
        option device 'radio0'
        option mode 'sta'
        option bssid '70:56:81:CD:1B:E5'
        option key 'R6WRL6B!'

ifconfig wlan0 up

########################
# USB
########################

# add USB support
opkg update
opkg install kmod-usb-storage block-mount block-hotplug kmod-fs-ext4
mkdir -p /mnt/usb
mount -t ext4 /dev/sda1 /mnt/usb -o rw,sync

# add USB to fstab (mount on start)
/etc/init.d/fstab start
/etc/init.d/fstab enable
/etc/init.d/fstab stop
uci add fstab mount
uci set fstab.@mount[-1].device=/dev/sda1
uci set fstab.@mount[-1].options=rw,sync
uci set fstab.@mount[-1].enabled_fsck=0
uci set fstab.@mount[-1].enabled=1
uci set fstab.@mount[-1].target=/mnt/usb
uci commit fstab
/etc/init.d/fstab start

# check if fstab is ok
cat /etc/fstab

# link the new USB to programs folder
ln -s /mnt/usb /opt

# add folder to install destination at the bottom of ```/etc/opkg.conf```
echo "dest usb /opt" >> /etc/opkg.conf

# Edit ```/etc/profile``` and add the new mount point to your paths variables:
echo "export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/opt/bin:/opt/sbin:/opt/usr/bin:/opt/usr/sbin" >> /etc/profile
echo "export LD_LIBRARY_PATH=/lib:/usr/lib:/usr/local/lib:/opt/lib:/opt/usr/lib" >> /etc/profile
source

# From here, you should be able to install new packages to your new mount point as follows:
# ex : opkg -dest usb install tcpdump  


########################
# Install programs
########################

# update sources
opkg update

# install programs
opkg install -dest usb netcat tcpdump aircrack-ng wireless-tools openvpn

# fix openVPN 
ln -s /mnt/usb/etc/init.d/openvpn /etc/init.d/openvpn
ln -s /mnt/usb/lib/modules/3.3.8/tun.ko /lib/modules/3.3.8/tun.ko
insmod tun

######################
# Install Justniffer SNIFF
######################

opkg install make
opkg install -dest usb patch tar

opkg install autotools libc6 libpcap0.8 g++ gcc libboost-iostreams libboost-program-options libboost-regex  

Collected errors:
 * opkg_install_cmd: Cannot install package autotools.
 * opkg_install_cmd: Cannot install package libc6.
 * opkg_install_cmd: Cannot install package libpcap0.8.
 * opkg_install_cmd: Cannot install package g++.
 * opkg_install_cmd: Cannot install package gcc.
 * opkg_install_cmd: Cannot install package libboost-iostreams.
 * opkg_install_cmd: Cannot install package libboost-program-options.
 * opkg_install_cmd: Cannot install package libboost-regex.

    $ ./configure 
    $ make 
    $ make install

    wget http://freefr.dl.sourceforge.net/project/justniffer/justniffer/justniffer%200.5.11/justniffer_0.5.11_i386.deb
    
    sudo apt-get install libboost-regex1.42.0 libboost-program-options1.42.0
    sudo dpkg -i justniffer_0.5.11_i386.deb


# If you install packages to external filesystems that have a startup script under /etc/init.d and (if enabled) under /etc/rc.d you need to set a sysmlink to /etc/init.d, e.g.:

    ln -s /usb/etc/init.d/openvpn /etc/init.d/openvpn

# Libraries installed along with those packages are also installed to the external filesystem. This causes the programm not to start during bootup. You need to manually set the LD_LIBRARY_PATH in each 'external' startup script:
    export LD_LIBRARY_PATH=/lib:/usr/lib:/tmp/lib:/tmp/usr/lib:/usb/lib:/usb/usr/lib




