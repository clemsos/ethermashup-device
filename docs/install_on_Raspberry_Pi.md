# Install on a Raspberry Pi

HOWTO Install Ether Mashup on a Raspberry Pi

## Setup the system

#### Install a Debian to your SD card

    dd=/path/to/raspbian_wheezy_20130917.img /dev/mmcbkl0

#### First Login
  

    ssh root@192.168.1.1
    #root password is : raspberry
    
    # add a user
    adduser pi
    # password is : raspberry

#### Configure install 

    dpkg-reconfigure tzdata # timezone
    dpkg-reconfigure locales # languages & locales : select en_US.UTF-8
    dpkg-reconfigure console-data # keyboard

#### Add network interfaces
  
    sudo nano /etc/network/interfaces

File 

    auto lo
    iface lo inet loopback
    
    # static ip 
    auto eth0
    iface eth0 inet static
      address 192.168.1.202
      netmask 255.255.255.0
      network 192.168.1.0
      broadcast 168.168.1.255
      gateway 192.168.1.254

    # wifi
    allow-hotplug wlan0
    iface wlan0 inet dhcp

#### Reboot
    
    reboot

#### Setup ssh user
    
    ssh-copy-id  pi@192.168.1.202
    ssh pi@192.168.1.202 # shouldn't ask for ssh password anymore

#### Prevent from asking sudo password (unsafe practice, but who cares?)

    # add user pi to sudoers

#### Connect to the Internet

    sudo nano /etc/network/interfaces

## Install the needed softwares 
  
    sudo apt-get update
    sudo apt-get install build-essential libpcap-dev libssl-dev netcat gcc g++ make autotools tar patch screen pv tshark git

    # Aircrack Suite
    # http://www.aircrack-ng.org/doku.php?id=install_aircrack#installing_aircrack-ng_from_source
    tar -xcf aircrack-ng-1.2-beta1.tar.gz
    ./configure
    make
    make install
    
    # Justniffer
    # http://justniffer.sourceforge.net/#!/install
    tar -xcf justniffer_0.5.11.tar.gz
    ./configure
    make
    make install

    # Libtins
    # http://libtins.sourceforge.net/installation/
    tar -xcf  libtins-1.1.tar.gz
    ./configure
    make
    make install
    
    # dot11decrypt 
    # http://average-coder.blogspot.fr/2013/06/decrypting-wepwpa2-traffic-on-fly.html
    tar -xcf dot11decrypt-master.zip
    ./configure
    make
    make install

## Configure sniffer

    git clone ethermashup-device
    ...
    chmod +x sniffer writer sender sender_secure
    

#### Add a startup script 

/etc/rc.local

    # /etc/rc.local
    sudo ifup wlan0
    sudo ifup wlan1
    

    # add dir & fix ownership
    mkdir -p /tmp/data
    touch /tmp/filelist
    sudo chown pi /tmp/data  /tmp/filelist