#!/bin/bash

# colors for fancy display :)
export GREEN="\\033[1;32m"
export NORMAL="\\033[0;39m"
export RED="\\033[1;31m"

# connect to wifi


# setup environment variables
## server IP
export ETHER_REMOTE_IP=127.0.0.1
## server port
export ETHER_REMOTE_PORT=9000
#server user
export ETHER_REMOTE_USER=user


# test internet connection
function check_internet_connection {
    ping -q -w 1 -c 1 `ip r | grep default | cut -d ' ' -f 3` > /dev/null && return 0 || return 1 
}
echo -e $NORMAL "Testing internet connection..."
check_internet_connection || (echo -e $RED "no network, bye" && exit 1)
echo -e $GREEN "OK!"

# ping the server
echo -e $NORMAL "Testing server connection..."
ping -q -w 1 -c 1 $ETHER_REMOTE_IP


# build up ssh connection
## generate key
# ssh-keygen

## send public key to the server
# cd ~/.ssh
# scp id_rsa.pub ${ETHER_REMOTE_USER}@${ETHER_REMOTE_IP} 

## ssh login into the server and send data
# http://blog.jetthoughts.com/2012/10/30/netcat-with-ssh-port-forwarding/

######################
# Monitoring mode
######################

# turn down all wifi interfaces
ifconfig wlan0 down

# add monitoring interface 
vi /etc/config/wireless

    config wifi-iface
        option device 'radio0'
        option ssid 'OpenWrt'
        option mode 'monitor'


# Test monitoring interface
tcpdump -i wlan0-1 -c 3

# create mon0 interface with all monitored content
airmon-ng start wlan0-1 1

####################################
# SELECT NETWORK
####################################

# FIND BSSID
airodump-ng wlan0-1

# convert ASCII WEP to HEX


# WEP 
############
airtun-ng -a BC:D1:77:21:1F:22  -w 65746865726d61736875703133 wlan0-1

# start decrypted interface
ifconfig at0 up

# start sniffing 
tcpdump -i at0 -c 5

# sending data to server


####################################
# GET IMAGES
####################################

justniffer-grab-http-traffic -d /home/clement/Dev/ethermashup/data -U clement -i at0
