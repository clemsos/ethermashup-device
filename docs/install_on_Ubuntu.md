##################################################################
# SETUP / INSTALL
#################################

# computer A
sudo apt-get install openssh-server openssh-client netcat pv screen
ssh-keygen -t rsa
# ssh-copy-id nico@nico1.local

# To OpenWRT router:
# ssh-copy-id root@192.168.1.1:/tmp/
# cd /etc/dropbear/
# cat /tmp/id*.pub >> authorized_keys
# chmod 0600 authorized_keys

# computer B : no password should be required to ssh at this point
ssh nico@nico1.local
# ssh root@192.168.1.1 

sudo apt-get install openssh-server openssh-client netcat pv screen aircrack-ng
ssh-keygen -t rsa
ssh-copy-id clement@192.168.1.1

# test copy
echo "this is a copy test" >> /tmp/blabla
./ncp.sh /tmp/blabla nico@nico.local:/tmp